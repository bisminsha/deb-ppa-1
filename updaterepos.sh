#!/bin/bash

#List of packages to skip
skip_pkg=("alacritty")
# Clone the repo
clone_url="$(jq -r '.project.git_http_url' /tmp/gitlab_payload.json)"
project_name=$(jq -r '.project.name' /tmp/gitlab_payload.json)

# Function to check if a package should be skipped
should_skip() {
    local pkg_file="$1"
    # Extract the package name from the .deb file using dpkg-deb
    local pkg_name=$(dpkg-deb --field "$pkg_file" Package)

    for skip in "${skip_pkg[@]}"; do
        if [[ "$pkg_name" == "$skip" ]]; then
            return 0 # True, should skip
        fi
    done
    return 1 # False, should not skip
}

mkdir -p pkgs/debs



echo $clone_url
git clone $clone_url
cd $project_name
chmod +x ./pack
sudo -u packager ./pack

# Copy entire contents of 'packages' to '/tmp/'
cp -r packages /tmp/

#Change to the parent directory
cd ../

# Set the path to your repository root
repo_root="pkgs/debs"

# Iterate over each directory under '/tmp/packages'
for dist_dir in /tmp/packages/*; do
    if [ -d "$dist_dir" ]; then
        dist_name=$(basename "$dist_dir")
        
        # Ensure the distribution directory has .deb files
        if [ -n "$(find "$dist_dir" -maxdepth 1 -type f -name '*.deb')" ]; then
            # Reprepro configuration
            mkdir -p "$repo_root/$dist_name"/conf
            touch "$repo_root/$dist_name"/conf/{options,distributions}

            # Configure distributions file for the current distribution
            cat <<EOL > "$repo_root/$dist_name"/conf/distributions
Origin: Webyfy
Label: Webyfy software Repository
Codename: $dist_name
Architectures: amd64
Components: main
Description: Webyfy software Repository for $dist_name
Suite: $dist_name
SignWith: FBD9D224A22E33B2BC6EF3B0EA1CFD05694C2A0A
EOL

            # Decrypt and extract keys
            openssl aes-256-cbc -d -pbkdf2 -in keys.tar.enc -out keys.tar -k "$KEY"
            tar -xf keys.tar
            gpg2 --import keys/pub.gpg && gpg2 --import keys/priv.gpg

            # Iterate over .deb files and include them if they are not in the skip list
            for deb_file in "$dist_dir"/*.deb; do
                if should_skip "$deb_file"; then
                    echo "Skipping package $(basename "$deb_file")."
                else
                    reprepro -V -b "$repo_root/$dist_name" -S main -P standard --ignore=forbiddenchar includedeb "$dist_name" "$deb_file"
                fi
            done
        else
            echo "No .deb files found in $dist_name directory."
        fi
    fi
done

echo -e "\e[0;32mDeploy to Gitlab Pages...\e[0m"
