# Webyfy-deb-repo

## How to install for **Debian/Ubuntu/Linux Mint**


- Add my key:

    ```sh
    sudo wget https://gitlab.com/bisminsha/deb-ppa-1/-/raw/main/pub.gpg \
    -O /usr/share/keyrings/webyfy-archive-keyring.asc
    ```

- Add the repository:

    - For Debian (bookworm)

        ```sh
        echo 'deb [ signed-by=/usr/share/keyrings/webyfy-archive-keyring.asc ] https://packages.webyfy.com/debs/bookworm bookworm main' \
        | sudo tee /etc/apt/sources.list.d/webyfy_softwares.list
        ```
    - For Ubuntu (Jammy)

        ```sh
        echo 'deb [ signed-by=/usr/share/keyrings/webyfy-archive-keyring.asc ] https://packages.webyfy.com/debs/jammy jammy main' \
        | sudo tee /etc/apt/sources.list.d/webyfy_softwares.list
        ```
- Update then install vscodium:

    ```sh
    sudo apt update
    sudo apt install <package_name>
    ```